<?php
defined('_JEXEC') or die;
include_once ('includes/functions.php');
include_once ('includes/includes.php');
require_once 'includes/Mobile_Detect.php';
$detect = new Mobile_Detect;
JHtml::_('jquery.framework');
JHtml::_('bootstrap.framework');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
  <?php if( $detect->isMobile() || $detect->isTablet() ){
      if(isset($_COOKIE['disableMobile'])){ ?>
        <?php if($_COOKIE['disableMobile']=='false'){ ?>
          <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1">
        <?php }
      }
      else { ?>
        <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1">
      <?php }
    }
    if ($hideByEdit == false){
      $doc->addStyleSheet('templates/'.$this->template.'/css/template.css');
      $doc->addStyleSheet('//fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic');
      if($themeLayout == 1){
        $doc->addStyleSheet('templates/'.$this->template.'/css/layout.css');
      }
    }
    else{
      $doc->addStyleSheet('media/jui/css/bootstrap.min.css');
      $doc->addStyleSheet('administrator/templates/'.$adminTemplate.'/css/template.css');
      $doc->addStyleSheet('templates/'.$this->template.'/css/edit.css');
      if($themeLayout == 1){
        $doc->addStyleSheet('templates/'.$this->template.'/css/layout.css');
      }
    }
  ?>
  <jdoc:include type="head" />
  <!--[if lt IE 9]>
    <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/ie.css" />
    <script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/html5shiv+printshiv.js"></script>
  <![endif]-->  
</head>
<body class="<?php echo $option . " view-" . $view . " task-" . $task . " itemid-" . $itemid . " body__" . $pageClass;?>">
  <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
      <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
        <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
      </a>
    </div>
  <![endif]-->
  <!-- Body -->
  <div id="wrapper">
    <div class="wrapper-inner">
      <?php if ($this->countModules('top') && $hideByEdit == false): ?>
      <!-- Top -->
      <div id="top-row">
        <div class="row-container">
          <div class="<?php echo $containerClass; ?>">
            <div id="top" class="<?php echo $rowClass; ?>">
              <jdoc:include type="modules" name="top" style="themeHtml5" />
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <!-- Header -->
      <?php if ($hideByEdit == false): ?>
      <div id="header-row">
        <header>
          <!-- Logo -->
          <div id="logo">
            <a href="<?php echo $this->baseurl; ?>">
              <h1><?php echo wrap_chars_with_span($sitename); ?></h1>
            </a>
          </div>
          <jdoc:include type="modules" name="header" style="html5nosize" />
        </header>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('slider') && $hideByView == false && $hideByEdit == false): ?>
      <!-- Slider -->
      <div id="slider-row">
        <jdoc:include type="modules" name="slider" style="html5nosize" />
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('navigation') && $hideByEdit == false): ?>
      <!-- Navigation -->
      <div id="navigation-row" role="navigation">
        <div class="row-container">
          <div class="<?php echo $containerClass; ?>">
            <div class="<?php echo $rowClass; ?>">
              <jdoc:include type="modules" name="navigation" style="themeHtml5" />
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('showcase') && $hideByView == false && $hideByEdit == false): ?>
      <!-- Showcase -->
      <div id="showcase-row">
        <div class="row-container">
          <div class="<?php echo $containerClass; ?>">
            <div class="<?php echo $rowClass; ?>">
                <jdoc:include type="modules" name="showcase" style="themeHtml5" />
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('feature') && $hideByView == false && $hideByEdit == false): ?>
      <!-- Feature -->
      <div id="feature-row">
        <div class="row-container">
          <div class="<?php echo $containerClass; ?>">
            <div class="<?php echo $rowClass; ?>">
                <jdoc:include type="modules" name="feature" style="themeHtml5" />
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('testimonials') && $hideByView == false && $hideByEdit == false): ?>
      <div id="testimonials-row"<?php if( !$detect->isMobile() && !$detect->isTablet() && ((int)$detect->version('IE') == '' || (int)$detect->version('IE') > 8 )){ ?> data-stellar-background-ratio="-0.28"<?php } ?>>
        <div class="row-container">
          <div class="<?php echo $containerClass; ?>">
            <div id="testimonials" class="<?php echo $rowClass; ?>">
              <jdoc:include type="modules" name="testimonials" style="themeHtml5" />
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('team') && $hideByView == false && $hideByEdit == false): ?>
      <div id="team-row">
        <div class="row-container">
          <div class="<?php echo $containerClass; ?>">
            <div id="team" class="<?php echo $rowClass; ?>">
              <jdoc:include type="modules" name="team" style="themeHtml5" />
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('priorities') && $hideByView == false && $hideByEdit == false): ?>
      <div id="priorities-row">
        <div class="row-container">
          <div class="<?php echo $containerClass; ?>">
            <div id="priorities" class="<?php echo $rowClass; ?>">
              <jdoc:include type="modules" name="priorities" style="themeHtml5" />
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('folio') && $hideByView == false && $hideByEdit == false): ?>
      <div id="folio-row"<?php if( !$detect->isMobile() && !$detect->isTablet() && ((int)$detect->version('IE') == '' || (int)$detect->version('IE') > 8 )){ ?> data-stellar-background-ratio="-0.43"<?php } ?>>
        <div class="row-container">
          <div class="<?php echo $containerClass; ?>">
            <div id="folio" class="<?php echo $rowClass; ?>">
              <jdoc:include type="modules" name="folio" style="themeHtml5" />
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('maintop') && $hideByView == false && $hideByEdit == false): ?>
      <!-- Maintop -->
      <div id="maintop-row">
        <div class="row-container">
          <div class="<?php echo $containerClass; ?>">
            <div id="maintop" class="<?php echo $rowClass; ?>">
              <jdoc:include type="modules" name="maintop" style="themeHtml5" />
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('video') && $hideByView == false && $hideByEdit == false): ?>
      <div id="video-row">
        <jdoc:include type="modules" name="video" style="html5nosize" />
      </div>
      <?php endif; ?>
      <div class="row-container">
        <div class="<?php echo $containerClass; ?>">
          <jdoc:include type="message" />
        </div>
      </div>
      <!-- Main Content row -->
      <div id="content-row">
        <div class="row-container">
          <div class="<?php echo $containerClass; ?>">
            <div class="content-inner <?php echo $rowClass; ?>">   
              <?php if ($this->countModules('aside-left') && ($hideByOption) == false && $view !== 'form' && $hideByEdit == false): ?>     
              <!-- Left sidebar -->
              <div id="aside-left" class="span<?php echo $asideLeftWidth; ?>">
                <aside role="complementary">
                  <jdoc:include type="modules" name="aside-left" style="html5nosize" />
                </aside>
              </div>
              <?php endif; ?>        
              <div id="component" class="span<?php echo $mainContentWidth; ?>">
                <main role="main">
                  <?php if ($this->countModules('breadcrumbs') && $layout !== 'edit'): ?>
                  <!-- Breadcrumbs -->
                  <div id="breadcrumbs-row">
                    <div id="breadcrumbs">
                      <jdoc:include type="modules" name="breadcrumbs" style="html5nosize" />
                    </div>
                  </div>
                  <?php endif; ?>       
                  <?php if ($this->countModules('content-top') && $hideByView == false && $hideByEdit == false): ?> 
                  <!-- Content-top -->
                  <div id="content-top-row" class="<?php echo $rowClass; ?>">
                    <div id="content-top">
                      <jdoc:include type="modules" name="content-top" style="themeHtml5" />
                    </div>
                  </div>
                  <?php endif; ?>        
                  <jdoc:include type="component" />   
                  <?php if ($this->countModules('content-bottom') && $hideByView == false && $hideByEdit == false): ?>     
                  <!-- Content-bottom -->
                  <div id="content-bottom-row" class="<?php echo $rowClass; ?>">
                    <div id="content-bottom">
                      <jdoc:include type="modules" name="content-bottom" style="themeHtml5" />
                    </div>
                  </div>
                  <?php endif; ?>
                </main>
              </div>        
              <?php if ($this->countModules('aside-right') && ($hideByOption) == false && $view !== 'form' && $hideByEdit == false): ?>
              <!-- Right sidebar -->
              <div id="aside-right" class="span<?php echo $asideRightWidth; ?>">
                <aside role="complementary">
                  <jdoc:include type="modules" name="aside-right" style="html5nosize" />
                </aside>
              </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <?php if ($this->countModules('blog') && $hideByView == false && $hideByEdit == false): ?>
          <jdoc:include type="modules" name="blog" style="html5nosize" />
        <?php endif; ?>
      </div>
      <?php if ($this->countModules('mainbottom') && $hideByView == false && $hideByEdit == false): ?>
      <!-- Mainbottom -->
      <div id="mainbottom-row">
        <div class="row-container">
          <div class="<?php echo $containerClass; ?>">
            <div id="mainbottom" class="<?php echo $rowClass; ?>">
              <jdoc:include type="modules" name="mainbottom" style="themeHtml5" />
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('bottom') && $hideByView == false && $hideByEdit == false): ?>
      <!-- Bottom -->
      <div id="bottom-row">
        <div class="row-container">
          <div class="<?php echo $containerClass; ?>">
            <div id="bottom" class="<?php echo $rowClass; ?>">
              <jdoc:include type="modules" name="bottom" style="themeHtml5" />
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('map') && $hideByView == false && $hideByEdit == false): ?>
      <!-- Footer -->
      <div id="map-row">
        <jdoc:include type="modules" name="map" style="html5nosize" />
      </div>
      <?php endif; ?>
      <?php if ($this->countModules('footer') && $hideByView == false && $hideByEdit == false): ?>
      <!-- Footer -->
      <div id="footer-row">
        <div class="row-container">
          <div class="<?php echo $containerClass; ?>">
            <div id="footer" class="<?php echo $rowClass; ?>">
              <jdoc:include type="modules" name="footer" style="themeHtml5" />
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <div id="push"></div>
    </div>
  </div>
  <?php if ($hideByEdit == false): ?>
  <div id="footer-wrapper">
    <div class="footer-wrapper-inner">
      <!-- Copyright -->
      <div id="copyright-row" role="contentinfo">
        <div id="copyright">
          <?php if($this->params->get('footerLogo') == 1) : ?>
          <!-- Footer Logo -->
          <a class="footer_logo" href="<?php echo $this->baseurl; ?>"><img src="<?php echo $footerLogo;?>" alt="<?php echo $sitename; ?>" /></a>
			    <?php else: ?>
          <span class="siteName"><?php echo $sitename; ?></span>
			    <?php endif; ?>
		      <?php if($this->params->get('footerCopy') == 1) echo '<span class="copy">&copy;</span>'; ?>
		      <?php if($this->params->get('footerYear') == 1) echo '<span class="year">'.date('Y').'</span>'; ?>
          <?php if($this->params->get('privacyLink') == 1) :?>
          <a class="privacy_link" href="<?php echo $privacy_link_url; ?>"><?php echo $this->params->get('privacy_link_title'); ?></a>
			    <?php endif; ?>
          <?php if($this->params->get('termsLink') == 1) :?>
          <a class="terms_link" href="<?php echo $terms_link_url; ?>"><?php echo $this->params->get('terms_link_title'); ?></a>
			    <?php endif; ?>
        </div>
        <jdoc:include type="modules" name="copyright" style="html5nosize" />
        <?php if($this->params->get('todesktop') && ($detect->isMobile() || $detect->isTablet())): ?>
        <div id="to-desktop">
          <a href="#">
            <span class="to_desktop"><?php echo $this->params->get('todesktop_text') ?></span>
            <span class="to_mobile"><?php echo $this->params->get('tomobile_text') ?></span>
          </a>
        </div>
        <?php endif; ?>
        <!-- {%FOOTER_LINK} -->
      </div>
    </div>
  </div>
  <?php if($this->params->get('totop')): ?>
  <div id="back-top">
    <a href="#"><i class="fa fa-arrow-circle-o-up"></i></a>
  </div>
  <?php endif; ?>
  <?php if ($this->countModules('modal')): ?>
  <jdoc:include type="modules" name="modal" style="modal" />
  <?php endif; ?>
  <jdoc:include type="modules" name="debug" style="none"/>
  <?php endif; ?>
  <?php  
    if ($hideByEdit == false){
      if ($this->countModules('modal')){ ?>
        <script src="<?php echo $this->baseurl.'/templates/'.$this->template.'/js/jquery.centerIn.js'; ?>"></script>
        <script>
          jQuery(function($) {
            $('.modal.loginPopup').alwaysCenterIn(window);
          });
        </script>
      <?php } ?>
      <?php if( $detect->isiPad() || $detect->isiPod()){
      if(isset($_COOKIE['disableMobile'])){ ?>
        <?php if($_COOKIE['disableMobile']=='false'){ ?>
          <script src="<?php echo $this->baseurl.'/templates/'.$this->template.'/js/ios-orientationchange-fix.js'; ?>"></script>
        <?php }
      }
      else { ?>
        <script src="<?php echo $this->baseurl.'/templates/'.$this->template.'/js/ios-orientationchange-fix.js'; ?>"></script>
      <?php }
    }
    if( $detect->isMobile() || $detect->isTablet() ){ ?>
    <script src="<?php echo $this->baseurl.'/templates/'.$this->template.'/js/desktop-mobile.js'; ?>"></script>
    <?php } ?>
    <script src="<?php echo JURI::base().'/templates/'.$this->template.'/js/jquery.modernizr.min.js'; ?>"></script>
    <?php if( !$detect->isMobile() && !$detect->isTablet() && ((int)$detect->version('IE') == '' || (int)$detect->version('IE') > 8 )){ ?>
    <script src="<?php echo JURI::base().'/templates/'.$this->template.'/js/jquery.stellar.min.js'; ?>"></script>
    <script>
      jQuery(function($) {
        if (!Modernizr.touch) {
          $(window).load(function(){
            $.stellar({responsive: true,horizontalScrolling: false});
          });
        }
      });
    </script>
    <?php }
    if( !$detect->isMobile() && !$detect->isTablet() && ((int)$detect->version('IE') == '' || (int)$detect->version('IE') > 8 ) && (!$detect->version('iOS') || $detect->version('iOS') !== "X")){ ?>
    <script src="<?php echo JURI::base().'/templates/'.$this->template.'/js/jquery.simplr.smoothscroll.min.js'; ?>"></script>
    <script>
      jQuery(function($) {
        if (!Modernizr.touch) {
          $.srSmoothscroll({ease: 'easeOutQuart'});
        }
      });
    </script>
    <?php }
      if($this->params->get('blackandwhite')): ?>
      <script src="<?php echo $this->baseurl.'/templates/'.$this->template.'/js/jquery.BlackAndWhite.min.js'; ?>"></script>
      <script>
        ;(function($, undefined) {
        $.fn.BlackAndWhite_init = function () {
          var selector = $(this);
          selector.not('.touchGalleryLink').BlackAndWhite({
            invertHoverEffect: ".$this->params->get('invertHoverEffect').",
            intensity: 1,
            responsive: true,
            speed: {
                fadeIn: ".$this->params->get('fadeIn').",
                fadeOut: ".$this->params->get('fadeOut')." 
            }
          });
        }
        })(jQuery);
        jQuery(window).load(function($){
          jQuery('.item_img a').each(function(){
            jQuery(this).find('img').not('.lazy').parent().BlackAndWhite_init();
          })
        });
      </script>
      <?php endif; ?>
      <script src="<?php echo $this->baseurl.'/templates/'.$this->template.'/js/jquery.vide.min.js'; ?>"></script>
      <script src="<?php echo $this->baseurl.'/templates/'.$this->template.'/js/scripts.js'; ?>"></script>
    <?php }
  ?>
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
 ga('create', 'UA-60438346-3', 'auto');
 ga('send', 'pageview');
 
</script>
</body>
</html>