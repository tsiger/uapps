<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="mod-custom mod-custom__<?php echo $moduleclass_sfx ?>" id="module_<?php echo $module->id; ?>">
	<div class="video-container">
		<div class="youtube" id="rYW9Y5oanUM" style="width:100%; height:1000px;"></div>
		<script src="/templates/theme2026/js/youtube.js" type="text/javascript"></script>
	</div>
	<!-- <div class="module-content">
		<div class="module-content-inner">
			<?php echo $module->content;?>
		</div>
	</div> -->
</div>
