#!/bin/bash
# Target directory

# Скрипт скопирует изменненые файлы (между указанными коммитами) со структурой директорий

# Нужно скопировать этот файл в директорию проекта
# Прежде чем запускать скрипт убедиться, что рабочая директория чистая - без измененных файлов, иначе измененные-незакоммиченные файлы могут попасть на выход

echo введите путь к папке куда будем сохранять изменения:
read TARGET

echo введите sha первого коммита:
read sha1
echo введите sha втогорого коммита:
read sha2

echo '\n'"Copied files:"
for i in $(git diff --name-only $sha1 $sha2)
    do
        # First create the target directory, if it doesn't exist.
        mkdir -p "$TARGET/$(dirname $i)"
        # Then copy over the file.
        cp -P "$i" "$TARGET/$i"
        echo "$i"
    done
